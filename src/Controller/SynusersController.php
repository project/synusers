<?php

namespace Drupal\synusers\Controller;

use Drupal\Core\Controller\ControllerBase;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\views\Views;

/**
 * Controller routines for page routes.
 */
class SynusersController extends ControllerBase {

  public function __construct() {
    $this->request = \Drupal::request()->query->all();
    $this->view_id = 'cml_users';
    $this->display_id = 'page';
    $this->args = [];
    $this->table = [];
    $this->result = [];
    $this->path = DRUPAL_ROOT . '/sites/default/files/Excel';
    $this->filename = 'users.xlsx';
  }

  /**
   * Constructs page from template.
   */
  public function page() {
    $this->getView();
    $this->xlsStructure();

    $headers = [
      'Content-Type'  => 'application/vnd.ms-excel',
      'Content-Disposition' => 'attachment;filename="' . $this->filename . '"',
    ];
    return new BinaryFileResponse($this->path . '/' . $this->filename, 200, $headers, TRUE);
  }

  /**
   * Guery execution.
   */
  public function getView() {
    $view = Views::getView($this->view_id);
    $view->setExposedInput($this->request);
    $view->setDisplay($this->display_id);
    $view->setArguments($this->args);
    $view->execute();
    if (!empty($view->result)) {
      foreach ($view->result as $rid => $row) {
        foreach ($view->field as $fid => $field) {
          if ($fid != 'operations' && $fid != 'status') {
            if ($rid == 0) {
              $this->table[] = $field->label();
            }
            $this->result[$rid][$fid] = $field->getValue($row);
          }
        }
      }
    }
  }

  /**
   * Xls.
   */
  public function xlsStructure() {
    if (!file_exists($this->path)) {
      mkdir($this->path, 0775);
    }
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->fromArray($this->table, NULL, 'A1', FALSE);
    $sheet->fromArray($this->result, NULL, 'A2', FALSE);
    $writer = new Xlsx($spreadsheet);
    $writer->save($this->path . '/' . $this->filename);
  }

}
